// Reference: https://github.com/Smithay/smithay/blob/master/smallvil/src/grabs/mod.rs
// Original file licensed under MIT, Copyright (c) 2017 Victor Berger and Victoria Brekenfeld
// Modified to adapt to this project, use a diff tool to see changes.

pub mod move_grab;
pub use move_grab::MoveSurfaceGrab;

pub mod resize_grab;
pub use resize_grab::ResizeSurfaceGrab;
