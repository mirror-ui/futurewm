use std::error::Error;
use std::sync::Arc;
use std::time::Instant;
use smithay::desktop::{PopupManager, Space, Window, WindowSurfaceType};
use smithay::input::{Seat, SeatState};
use smithay::reexports::calloop::{EventLoop, Interest, LoopHandle, LoopSignal, Mode, PostAction};
use smithay::reexports::calloop::generic::Generic;
use smithay::reexports::wayland_server::backend::{ClientData, ClientId, DisconnectReason};
use smithay::reexports::wayland_server::{Display, DisplayHandle};
use smithay::reexports::wayland_server::protocol::wl_surface::WlSurface;
use smithay::utils::{Logical, Point};
use smithay::wayland::compositor::{CompositorClientState, CompositorState};
use smithay::wayland::output::OutputManagerState;
use smithay::wayland::selection::data_device::DataDeviceState;
use smithay::wayland::shell::xdg::XdgShellState;
use smithay::wayland::shm::ShmState;
use smithay::wayland::socket::ListeningSocketSource;
use crate::FutureCompositorData;

pub struct FutureCompositorState {
    pub start_time: Instant,

    pub socket_name: String,

    pub handle: LoopHandle<'static, FutureCompositorData>,
    pub display_handle: DisplayHandle,
    pub loop_signal: LoopSignal,

    pub compositor_state: CompositorState,
    pub xdg_shell_state: XdgShellState,
    pub shm_state: ShmState,
    pub output_manager_state: OutputManagerState,
    pub seat_state: SeatState<FutureCompositorState>,
    pub data_device_state: DataDeviceState,

    pub popups: PopupManager,
    pub seat: Seat<Self>,
    pub space: Space<Window>,
}

#[derive(Default)]
pub struct ClientState {
    pub compositor_state: CompositorClientState,
}


impl FutureCompositorState {

    pub fn new(
        event_loop: &mut EventLoop<'static, FutureCompositorData>,
        display: Display<Self>
    ) -> Result<Self, Box<dyn Error>> {
        let start_time = std::time::Instant::now();

        let display_handle = display.handle();

        let compositor_state = CompositorState::new::<Self>(&display_handle);
        let xdg_shell_state = XdgShellState::new::<Self>(&display_handle);
        let shm_state = ShmState::new::<Self>(&display_handle, vec![]);
        let output_manager_state =
            OutputManagerState::new_with_xdg_output::<Self>(&display_handle);
        let mut seat_state = SeatState::new();
        let data_device_state = DataDeviceState::new::<Self>(&display_handle);
        let popups = PopupManager::default();

        let mut seat: Seat<Self> = seat_state.new_wl_seat(&display_handle, "winit");
        seat.add_keyboard(Default::default(), 400, 40).unwrap();
        seat.add_pointer();

        let space = Space::default();

        let socket_name = Self::init_wayland_listener(display, event_loop)?;

        let loop_signal = event_loop.get_signal();
        let handle = event_loop.handle();

        Ok(Self {
            start_time,
            display_handle,

            socket_name,

            handle,
            loop_signal,

            compositor_state,
            xdg_shell_state,
            shm_state,
            output_manager_state,
            seat_state,
            data_device_state,

            popups,
            seat,
            space,
        })
    }

    fn init_wayland_listener(
        display: Display<FutureCompositorState>,
        event_loop: &mut EventLoop<FutureCompositorData>,
    ) -> Result<String, Box<dyn Error>> {
        let listening_socket = ListeningSocketSource::new_auto().unwrap();
        let socket_name = listening_socket.socket_name().to_os_string();

        let handle = event_loop.handle();

        event_loop
            .handle()
            .insert_source(listening_socket, move |client_stream, _, state| {
                let mut display_handle = state
                    .compositor
                    .display_handle.lock().unwrap();
                display_handle
                    .insert_client(client_stream, Arc::new(ClientState::default()))
                    .unwrap();
            })?;

        handle
            .insert_source(
                Generic::new(display, Interest::READ, Mode::Level),
                |_, display, data| {
                    let mut state = data.compositor.state.lock().unwrap();
                    // Safety: we don't drop the display (except if you're Linus)
                    unsafe {
                        display.get_mut().dispatch_clients(&mut state).unwrap();
                    }
                    Ok(PostAction::Continue)
                },
            )?;

        Ok(
            socket_name.to_str().ok_or_else(|| format!("unable to convert socket name"))?
                .to_string()
        )
    }

    pub fn surface_under(&self, pos: Point<f64, Logical>) -> Option<(WlSurface, Point<i32, Logical>)> {
        self.space.element_under(pos).and_then(|(window, location)| {
            window
                .surface_under(pos - location.to_f64(), WindowSurfaceType::ALL)
                .map(|(s, p)| (s, p + location))
        })
    }

}

impl ClientData for ClientState {
    fn initialized(&self, _client_id: ClientId) {}
    fn disconnected(&self, _client_id: ClientId, _reason: DisconnectReason) {}
}
