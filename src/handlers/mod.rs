// Reference: https://github.com/Smithay/smithay/blob/7aff4d78fa60a6f2f3857d651c5f4f0cee58c63b/smallvil/src/handlers/mod.rs
// Original file licensed under MIT, Copyright (c) 2017 Victor Berger and Victoria Brekenfeld
// Modified to adapt to this project, use a diff tool to see changes.

mod compositor;
mod xdg_shell;

use crate::FutureCompositorState;

use smithay::input::{Seat, SeatHandler, SeatState};
use smithay::reexports::wayland_server::protocol::wl_surface::WlSurface;
use smithay::reexports::wayland_server::Resource;
use smithay::wayland::selection::data_device::{
    set_data_device_focus, ClientDndGrabHandler, DataDeviceHandler, DataDeviceState, ServerDndGrabHandler,
};
use smithay::wayland::selection::SelectionHandler;
use smithay::{delegate_data_device, delegate_output, delegate_seat};

impl SeatHandler for FutureCompositorState {
    type KeyboardFocus = WlSurface;
    type PointerFocus = WlSurface;

    fn seat_state(&mut self) -> &mut SeatState<FutureCompositorState> {
        &mut self.seat_state
    }

    fn focus_changed(&mut self, seat: &Seat<Self>, focused: Option<&WlSurface>) {
        let dh = &self.display_handle;
        let client = focused.and_then(|s| dh.get_client(s.id()).ok());
        set_data_device_focus(dh, seat, client);
    }

    fn cursor_image(&mut self, _seat: &Seat<Self>, _image: smithay::input::pointer::CursorImageStatus) {}
}

delegate_seat!(FutureCompositorState);

impl SelectionHandler for FutureCompositorState {
    type SelectionUserData = ();
}

impl DataDeviceHandler for FutureCompositorState {
    fn data_device_state(&self) -> &DataDeviceState {
        &self.data_device_state
    }
}

impl ClientDndGrabHandler for FutureCompositorState {}
impl ServerDndGrabHandler for FutureCompositorState {}

delegate_data_device!(FutureCompositorState);
delegate_output!(FutureCompositorState);
