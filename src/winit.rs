// Reference: https://github.com/Smithay/smithay/blob/7aff4d78fa60a6f2f3857d651c5f4f0cee58c63b/smallvil/src/winit.rs
// Original file licensed under MIT, Copyright (c) 2017 Victor Berger and Victoria Brekenfeld
// Modified to adapt to this project, use a diff tool to see changes.

#![cfg(feature = "winit")]

use std::error::Error;
use std::sync::Arc;
use std::time::Duration;

use smithay::{
    backend::{
        renderer::{
            damage::OutputDamageTracker, element::surface::WaylandSurfaceRenderElement, gles::GlesRenderer,
        },
        winit::{self, WinitEvent},
    },
    output::{Mode, Output, PhysicalProperties, Subpixel},
    reexports::calloop::EventLoop,
    utils::{Rectangle, Transform},
};

use crate::{FutureCompositorState, FutureCompositorData, FutureCompositor};

pub fn init_winit(
    event_loop: &mut EventLoop<FutureCompositorData>,
    compositor: Arc<FutureCompositor>,
) -> Result<(), Box<dyn Error>> {
    let display_handle = compositor.display_handle.lock().unwrap();
    let mut state = compositor.state.lock().unwrap();

    let (mut backend, winit) = winit::init()?;

    let mode = Mode {
        size: backend.window_size(),
        refresh: 60_000,
    };

    let output = Output::new(
        "winit".to_string(),
        PhysicalProperties {
            size: (0, 0).into(),
            subpixel: Subpixel::Unknown,
            make: "FutureWM".into(),
            model: "Winit".into(),
        },
    );
    let _global = output.create_global::<FutureCompositorState>(&display_handle);
    output.change_current_state(Some(mode), Some(Transform::Flipped180), None, Some((0, 0).into()));
    output.set_preferred(mode);

    state.space.map_output(&output, (0, 0));

    let mut damage_tracker = OutputDamageTracker::from_output(&output);

    std::env::set_var("WAYLAND_DISPLAY", &state.socket_name);

    event_loop.handle().insert_source(winit, move |event, _, data| {
        let mut display = data.compositor.display_handle.lock().unwrap();
        let mut state = data.compositor.state.lock().unwrap();

        match event {
            WinitEvent::Resized { size, .. } => {
                output.change_current_state(
                    Some(Mode {
                        size,
                        refresh: 60_000,
                    }),
                    None,
                    None,
                    None,
                );
            }
            WinitEvent::Input(event) => state.process_input_event(event),
            WinitEvent::Redraw => {
                let size = backend.window_size();
                let damage = Rectangle::from_loc_and_size((0, 0), size);

                backend.bind().unwrap();
                smithay::desktop::space::render_output::<_, WaylandSurfaceRenderElement<GlesRenderer>, _, _>(
                    &output,
                    backend.renderer(),
                    1.0,
                    0,
                    [&state.space],
                    &[],
                    &mut damage_tracker,
                    [0.1, 0.1, 0.1, 1.0],
                )
                .unwrap();
                backend.submit(Some(&[damage])).unwrap();

                state.space.elements().for_each(|window| {
                    window.send_frame(
                        &output,
                        state.start_time.elapsed(),
                        Some(Duration::ZERO),
                        |_, _| Some(output.clone()),
                    )
                });

                state.space.refresh();
                state.popups.cleanup();
                let _ = display.flush_clients();

                // Ask for redraw to schedule new frame.
                backend.window().request_redraw();
            }
            WinitEvent::CloseRequested => {
                state.loop_signal.stop();
            }
            _ => (),
        };
    })?;

    Ok(())
}
