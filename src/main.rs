mod handlers;

mod grabs;
mod input;
mod state;
mod winit;
mod drm;

use std::error::Error;
use std::sync::{Arc, Mutex};
use smithay::reexports::{calloop, calloop::EventLoop, wayland_server::{Display, DisplayHandle}};
pub use state::FutureCompositorState;

#[cfg(feature = "drm")]
use {
    crate::drm::{DrmState, init_drm},
    std::sync::OnceLock
};

#[cfg(feature = "winit")]
use {
    crate::winit::init_winit
};
use log::{info, warn};

pub struct FutureCompositorData {
    compositor: Arc<FutureCompositor>
}

impl From<Arc<FutureCompositor>> for FutureCompositorData {
    fn from(value: Arc<FutureCompositor>) -> Self {
        FutureCompositorData { compositor: value }
    }
}

pub struct FutureCompositor {
    pub state: Mutex<FutureCompositorState>,
    display_handle: Mutex<DisplayHandle>,

    #[cfg(feature = "drm")]
    pub drm_state: OnceLock<Mutex<DrmState>>
}

impl FutureCompositor {
    pub fn new(event_loop: &mut EventLoop<'static, FutureCompositorData>) -> Result<Self, Box<dyn Error>> {
        let display: Display<FutureCompositorState> = Display::new()?;
        let display_handle = display.handle();
        let state = FutureCompositorState::new(event_loop, display)?;
        Ok(Self {
            state: Mutex::new(state),
            display_handle: Mutex::new(display_handle),

            #[cfg(feature = "drm")]
            drm_state: OnceLock::new(),
        })
    }

    #[cfg(feature = "winit")]
    pub fn init_winit(self: Arc<FutureCompositor>, event_loop: &mut EventLoop<FutureCompositorData>) -> Result<(), Box<dyn Error>> {
        init_winit(event_loop, self)
    }

    #[cfg(feature = "drm")]
    pub fn init_drm(self: Arc<FutureCompositor>, event_loop: &mut EventLoop<FutureCompositorData>) -> Result<(), Box<dyn Error>> {
        init_drm(event_loop, self)
    }

    pub fn run(self: Arc<FutureCompositor>, event_loop: &mut EventLoop<FutureCompositorData>) -> calloop::error::Result<()> {
        event_loop.run(None, &mut self.into(), move |_| {
            info!("Compositor is running");
            if let Err(e) = std::process::Command::new("weston-terminal").spawn() {
                warn!("Could not spawn process: {}", e)
            }
        })
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    info!("Starting {}", env!("CARGO_PKG_NAME"));
    if let Ok(env_filter) = tracing_subscriber::EnvFilter::try_from_default_env() {
        tracing_subscriber::fmt().with_env_filter(env_filter).init();
    } else {
        tracing_subscriber::fmt().init();
    }

    info!("Creating compositor");
    let mut event_loop = EventLoop::try_new()?;
    let compositor = Arc::new(FutureCompositor::new(&mut event_loop)?);

    #[cfg(feature = "winit")] {
        info!("Initializing winit");
        compositor.clone().init_winit(&mut event_loop)?;
    }

    #[cfg(feature = "drm")] {
        info!("Initializing DRM");
        compositor.clone().init_drm(&mut event_loop)?;
    }

    info!("Compositor is ready!");
    compositor.run(&mut event_loop)?;

    Ok(())
}
