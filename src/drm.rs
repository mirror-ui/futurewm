// Reference: https://github.com/Smithay/smithay/blob/7aff4d78fa60a6f2f3857d651c5f4f0cee58c63b/smithay-drm-extras/examples/simple.rs
// Original file licensed under MIT, Copyright (c) 2017 Victor Berger and Victoria Brekenfeld
// Modified to adapt to this project, use a diff tool to see changes.

#![cfg(feature = "drm")]

use std::{collections::HashMap, path::PathBuf, time::Duration};
use std::error::Error;
use std::sync::{Arc, Mutex};
use log::{info, warn};

use smithay_drm_extras::{
    drm_scanner::{self, DrmScanEvent},
    edid::EdidInfo,
};

use smithay::{
    backend::{
        drm::{self, DrmDeviceFd, DrmNode},
        session::{libseat::LibSeatSession, Session},
        udev::{UdevBackend, UdevEvent},
    },
    reexports::{
        calloop::{timer::Timer, EventLoop},
        rustix::fs::OFlags,
    },
    utils::DeviceFd,
};
use smithay::reexports::drm::control::{connector, crtc};
use crate::{FutureCompositor, FutureCompositorData};

pub struct DrmState {
    session: LibSeatSession,
    devices: HashMap<DrmNode, Device>,
    compositor: Arc<FutureCompositor>,
}

struct Device {
    drm: drm::DrmDevice,
    drm_scanner: drm_scanner::DrmScanner,
    surfaces: HashMap<crtc::Handle, Surface>,
}

#[derive(Clone)]
struct Surface {
    // Your gbm surface stuff goes here
}

pub fn init_drm(
    event_loop: &mut EventLoop<FutureCompositorData>,
    compositor: Arc<FutureCompositor>
) -> Result<(), Box<dyn Error>> {
    info!("Obtaining seat");
    let (session, notify) = LibSeatSession::new().unwrap();

    event_loop.handle().insert_source(notify, |_, _, _| {}).unwrap();

    compositor.drm_state.set(Mutex::new(DrmState {
        session,
        devices: Default::default(),
        compositor: compositor.clone(),
    })).map_err(|_| format!("DRM already initialized!"))?;

    init_udev(compositor);

    event_loop
        .handle()
        .insert_source(Timer::from_duration(Duration::from_secs(5)), |_, _, _| {
            panic!("Aborted");
        })
        .unwrap();

    Ok(())
}

fn init_udev(compositor: Arc<FutureCompositor>) {
    info!("Initializing udev");

    let mut drm_state = compositor.drm_state.get().unwrap().lock().unwrap();
    let backend = UdevBackend::new(drm_state.session.seat()).unwrap();
    for (device_id, path) in backend.device_list() {
        drm_state.on_udev_event(UdevEvent::Added {
            device_id,
            path: path.to_owned(),
        });
    }

    let state = compositor.state.lock().unwrap();
    state.handle
        .insert_source(backend, |event, _, state| {
            let mut drm_state = state.compositor.drm_state.get().unwrap().lock().unwrap();
            drm_state.on_udev_event(event)
        })
        .unwrap();
}

impl DrmState {
    fn on_udev_event(&mut self, event: UdevEvent) {
        match event {
            UdevEvent::Added { device_id, path } => {
                if let Ok(node) = DrmNode::from_dev_id(device_id) {
                    info!(
                        "Found device {} {} at {}",
                        device_id, node.to_string(), path.to_string_lossy()
                    );
                    match self.device_added(node, path.clone()) {
                        Ok(()) => {
                            info!(
                                "Successfully opened card {} at {}",
                                node.to_string(), path.to_string_lossy()
                            )
                        },
                        Err(e) => {
                            warn!(
                                "Failed to open card {} at {}: {}, waiting for more cards",
                                node.to_string(), path.to_string_lossy(), e
                            )
                        }
                    }
                }
            }
            UdevEvent::Changed { device_id } => {
                if let Ok(node) = DrmNode::from_dev_id(device_id) {
                    self.device_changed(node);
                }
            }
            UdevEvent::Removed { device_id } => {
                if let Ok(node) = DrmNode::from_dev_id(device_id) {
                    self.device_removed(node);
                }
            }
        }
    }

    fn device_added(&mut self, node: DrmNode, path: PathBuf) -> Result<(), Box<dyn Error>>{
        info!("Adding device {} at {}", node.to_string(), path.to_string_lossy());
        let fd = self
            .session
            .open(
                &path,
                OFlags::RDWR | OFlags::CLOEXEC | OFlags::NOCTTY | OFlags::NONBLOCK,
            )?;

        info!("Opening DRM device");
        let fd = DrmDeviceFd::new(DeviceFd::from(fd));

        let (drm, drm_notifier) = drm::DrmDevice::new(fd, false)?;

        {
            let state = self.compositor.state.lock().unwrap();
            state.handle
                .insert_source(drm_notifier, move |event, _, _| match event {
                    drm::DrmEvent::VBlank(_) => {}
                    drm::DrmEvent::Error(_) => {}
                })?;
        }

        self.devices.insert(
            node,
            Device {
                drm,
                drm_scanner: Default::default(),
                surfaces: Default::default(),
            },
        );

        self.device_changed(node);

        Ok(())
    }

    fn connector_connected(&mut self, node: DrmNode, connector: connector::Info, crtc: crtc::Handle) {
        if let Some(device) = self.devices.get_mut(&node) {
            let name = format!("{}-{}", connector.interface().as_str(), connector.interface_id());

            let (manufacturer, model) = EdidInfo::for_connector(&device.drm, connector.handle())
                .map(|info| (info.manufacturer, info.model))
                .unwrap_or_else(|| ("Unknown".into(), "Unknown".into()));

            println!("Connected:");
            dbg!(name);
            dbg!(manufacturer);
            dbg!(model);

            device.surfaces.insert(crtc, Surface {});
        }
    }

    fn connector_disconnected(&mut self, node: DrmNode, connector: connector::Info, crtc: crtc::Handle) {
        let name = format!("{}-{}", connector.interface().as_str(), connector.interface_id());

        println!("Disconnected:");
        dbg!(name);

        if let Some(device) = self.devices.get_mut(&node) {
            device.surfaces.remove(&crtc);
        }
    }

    fn device_changed(&mut self, node: DrmNode) {
        let device = if let Some(device) = self.devices.get_mut(&node) {
            device
        } else {
            return;
        };

        for event in device.drm_scanner.scan_connectors(&device.drm) {
            match event {
                DrmScanEvent::Connected {
                    connector,
                    crtc: Some(crtc),
                } => {
                    info!("DRM connected: {}", node.to_string());
                    self.connector_connected(node, connector, crtc);
                }
                DrmScanEvent::Disconnected {
                    connector,
                    crtc: Some(crtc),
                } => {
                    self.connector_disconnected(node, connector, crtc);
                }
                _ => {}
            }
        }
    }

    fn device_removed(&mut self, node: DrmNode) {
        let device = if let Some(device) = self.devices.get_mut(&node) {
            device
        } else {
            return;
        };

        let crtcs: Vec<_> = device
            .drm_scanner
            .crtcs()
            .map(|(info, crtc)| (info.clone(), crtc))
            .collect();

        for (connector, crtc) in crtcs {
            self.connector_disconnected(node, connector, crtc);
        }

        self.devices.remove(&node);
    }
}